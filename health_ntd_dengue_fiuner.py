# -*- coding: utf-8 -*-
##############################################################################
#
#    GNU Health: The Free Health and Hospital Information System
#    Copyright (C) 2008-2020 Luis Falcon <lfalcon@gnusolidario.org>
#    Copyright (C) 2011-2020 GNU Solidario <health@gnusolidario.org>
#
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from trytond.model import ModelView, ModelSQL, fields, Unique
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Not, Bool, Eval


__all__ = ['DengueDUSurveyOvitrap','DengueDUSurvey']


class DengueDUSurveyOvitrap(ModelSQL, ModelView):
    'Dengue DU SUrvey Ovitraps'
    __name__ = 'gnuhealth.dengue_du_survey.ovitrap'
    
    name = fields.Char("Ovitrap Code", required = True)    
    material = fields.Char("Material")
    
    def get_rec_name(self, name):
        return self.name
    
    @classmethod
    def search_rec_name(cls, name, clause):
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'
        return [bool_op,
            ('name',) + tuple(clause[1:]),
            ]
    
    @classmethod
    def __setup__(cls):
        super(DengueDUSurveyOvitrap, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints = [
            ('name_unique', Unique(t,t.name),
                'The name of the ovitrap has to be unique. Check the records'),
            ] 

class DengueDUSurvey(metaclass = PoolMeta):
    'Dengue DU Survey'
    __name__ = 'gnuhealth.dengue_du_survey'
    
    ovitrap = fields.Many2One('gnuhealth.dengue_du_survey.ovitrap',"Ovitrap")
    placement_date = fields.Date("Placement Date", help="When it has been placed the ovitrap.",
                                 states={
                                     'readonly': Not(Bool(Eval('ovitrap'))),
                                     'required': Bool(Eval('ovitrap')),
                                     })
    retirement_date = fields.Date("Retirement Date", help="When it has been retired the ovitrap",
                                  states={
                                      'readonly': Not(Bool(Eval('placement_date'))),
                                      })
    condition = fields.Selection([
        (None,''),
        ('good', 'Good'),
        ('broken', 'Broken'),
        ('altered', 'Altered'),
        ('missing', 'Missing'),
        ], 'Condition', sort=False,
        states={
                'readonly': Not(Bool(Eval('retirement_date'))),
                'required': Bool(Eval('retirement_date')),
                })
    comments = fields.Text ("Comments",
        states={
            'readonly': Not(Bool(Eval('ovitrap'))),
            })
    epidemiological_week = fields.Function(
        fields.Integer('Epidemiological Week'),
        'get_ep_week')
    first_read = fields.Integer ("First Read", help="",
        states={
            'readonly': Not(Bool(Eval('retirement_date'))),
            })
    second_read = fields.Integer("Second Read",
        states={
            'readonly': Not(Bool(Eval('retirement_date'))),
            })
    responsible_person = fields.Many2One(
        'party.party', 'Responsible Person',
        domain=[('is_person', '=', True)],
        states={
            'readonly': Not(Bool(Eval('ovitrap'))),
            'required': Bool(Eval('ovitrap')),
            })
    operational_sector = fields.Function(fields.Many2One(
        'gnuhealth.operational_sector', 'Op.Sector',
        help="Operational / Sanitary region"),
        'get_operational_sector')

    def get_ep_week(self, name):
        return self.on_change_with_epidemiological_week()
    
    @fields.depends('retirement_date')
    def on_change_with_epidemiological_week(self):
        if self.retirement_date:
            return self.retirement_date.isocalendar()[1]
        return None
    
    def get_operational_sector(self, name):
        return self.on_change_with_operational_sector()

    @fields.depends('du')
    def on_change_with_operational_sector(self):
        if (self.du and self.du.operational_sector):
            return (self.du.operational_sector.id)

    @classmethod
    def search_rec_name(cls, name, clause):
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'
        return [bool_op,
            ('du',) + tuple(clause[1:]),
            ('ovitrap.name',) + tuple(clause[1:]),
            ]
    
    @classmethod
    def create(cls, vlist):
        pool = Pool()
        DDUSurvey = pool.get('gnuhealth.dengue_du_survey')
        ovitraps = [x['ovitrap'] for x in vlist if x['ovitrap'] != None]
        dus = [x['du'] for x in vlist if x['du'] != None]
        dengue_du_surveys = []
        if len(ovitraps)>0:
            dengue_du_surveys = DDUSurvey.search([
                                    ('ovitrap','in',ovitraps),
                                    ('retirement_date','=',None),
                                    ])
        if len(dengue_du_surveys)>0:
            cls.raise_user_error('ovitrap_inplaced',{
                        'survey_sequence': ','.join([x.name for x in dengue_du_surveys])
                        })
        dengue_du_surveys = []
        if len(dus)>0:
            dengue_du_surveys = DDUSurvey.search([
                                    ('du','in',dus),
                                    ('ovitrap','!=',None),
                                    ('retirement_date','=',None),
                                    ])
        if len(dengue_du_surveys)>0:
            cls.raise_user_warning(
                        'survey'+','.join([x.name for x in dengue_du_surveys]),
                        'du_already_has_ovitrap',{
                        'survey_sequence': ','.join([x.name for x in dengue_du_surveys])},
                        'Do you want to place another ovitrap on the same du?'
                        )
        return super(DengueDUSurvey, cls).create(vlist)
    
    @classmethod
    def write(cls, *args):
        pool = Pool()
        DDUSurvey = pool.get('gnuhealth.dengue_du_survey')        
        actions = iter(args)
        for dengue_du_surveys, values in zip(actions,actions):
            '''Check if the ovitrap is not on another du'''
            if 'ovitrap' in values and values['ovitrap'] != None:
                surveys = DDUSurvey.search([
                            ('ovitrap','=',values['ovitrap']),
                            ('retirement_date','=',None),
                            ])
                if len(surveys)>0 and set(dengue_du_surveys)^set(surveys):
                    cls.raise_user_error('ovitrap_inplaced',{
                        'survey_sequence': ','.join([x.name for x in surveys])
                        })
            '''Check if the du doesn't have another ovitrap'''
            if 'du' in values and values['du'] != None:
                surveys = DDUSurvey.search([
                                    ('du','=',values['du']),
                                    ('ovitrap','!=',None),
                                    ('retirement_date','=',None),
                                    ])
                if len(surveys)>0 and set(dengue_du_surveys)^set(surveys):
                    cls.raise_user_warning(
                        'survey'+','.join([x.name for x in surveys]),
                        'du_already_has_ovitrap',{
                        'survey_sequence': ','.join([x.name for x in surveys])},
                        'Do you want to place another ovitrap on the same du?'
                        )
        super(DengueDUSurvey, cls).write(*args)

    @classmethod
    def __setup__(cls):
        super(DengueDUSurvey, cls).__setup__()        
        cls._error_messages.update({
            'ovitrap_inplaced': (
                'Ovitrap already inplaced. Check "%(survey_sequence)s"'),
            'du_already_has_ovitrap':(
                'This du has already an ovitrap inplaced. Check "%(survey_sequence)s"'),
            })
        t = cls.__table__()
        cls._sql_constraints = [
            ('placement_unique', Unique(t,t.placement_date, t.ovitrap),
                'This ovitrap has been already placed. Check the records'),
            ]
        cls._sql_constraints = [
            ('check_unique', Unique(t,t.retirement_date, t.ovitrap),
                'This ovitrap has been already checked. Check the records'),
            ]
        
        cls.du.required = True
